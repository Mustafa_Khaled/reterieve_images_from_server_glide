package com.example.khaleds.testconnection;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;

public class VuewFullImage extends AppCompatActivity {
    private ImageView imageView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vuew_full_image);
        Intent intent = getIntent();
        String i = intent.getStringExtra(MainActivity.BITMAP_ID);
        imageView = (ImageView) findViewById(R.id.imageview);
        Glide.with(this)
                .load(i)
                .centerCrop()
                .placeholder(R.drawable.ic_launcher_background)
                .crossFade()
                .into(imageView);
    }
}
