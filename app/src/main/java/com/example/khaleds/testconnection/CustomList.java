package com.example.khaleds.testconnection;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Khaled's on 3/25/2018.
 */

public class CustomList extends ArrayAdapter<ImageModel> {
    Context context;
    int resource;
    ArrayList<ImageModel> arrayList;
    public static ArrayList<String> al=new ArrayList<>();


    public CustomList(Context context, int resource, ArrayList<ImageModel> objects) {
        super(context, resource, objects);
        this.arrayList = objects;
        this.context = context;
        this.resource = resource;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) getContext()
                    .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.row_item, null, true);

        }
        ImageModel product = getItem(position);

        ImageView imageView = (ImageView) convertView.findViewById(R.id.imageview);
        Glide.with(context)
                .load(product.getImageurl())
                .centerCrop()
                .placeholder(R.drawable.ic_launcher_background)
                .crossFade()
                .into(imageView);

        al.add(product.getImageurl());
        TextView txtName = (TextView) convertView.findViewById(R.id.url);
        txtName.setText(product.getName());


        return convertView;
    }
}