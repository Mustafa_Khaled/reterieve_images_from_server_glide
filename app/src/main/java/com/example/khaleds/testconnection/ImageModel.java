package com.example.khaleds.testconnection;

/**
 * Created by Khaled's on 3/25/2018.
 */

public class ImageModel {
    public String getImageurl() {
        return imageurl;
    }

    public void setImageurl(String imageurl) {
        this.imageurl = imageurl;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    String imageurl;
    String name;

    public ImageModel(String imageurl, String name) {
        this.imageurl = imageurl;
        this.name = name;
    }
}
